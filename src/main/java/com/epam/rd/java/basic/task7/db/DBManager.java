package com.epam.rd.java.basic.task7.db;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.*;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {

    private static DBManager instance;

    private static final Properties properties = new Properties();

    private static Connection con;

    private static final String BAD_INPUT = "Bad input.";

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            try {
                instance = new DBManager();
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                con = DriverManager.getConnection(properties.getProperty("connection.url"));
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return instance;
    }

    public Connection getConnection() {
        return con;
    }

    private DBManager() throws IOException {
        try (InputStream in = new FileInputStream("app.properties")) {
            properties.load(in);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<User> findAllUsers() throws DBException {
        final List<User> all = new ArrayList<>();
        try (PreparedStatement statement = con.prepareStatement("SELECT * FROM USERS")) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                final int userId = resultSet.getInt("ID");
                final String login = resultSet.getString("LOGIN");
                User user = User.createUser(login);
                user.setId(userId);
                all.add(user);
            }
            return all;
        } catch (SQLException e) {
            throw new DBException("Failed to get all user", e);
        }
    }

    public boolean insertUser(final User user) throws DBException {
        if (user == null) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        try (PreparedStatement statement = con.prepareStatement(
                "INSERT INTO USERS (LOGIN) "
                        + "VALUES(?)", Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, user.getLogin());
            statement.executeUpdate();
            final ResultSet primaryKeys = statement.getGeneratedKeys();
            if (primaryKeys.next()) {
                user.setId(primaryKeys.getInt(1));
            }
        } catch (SQLException e) {
            throw new DBException("Failed to create new user" + user.getLogin(), e);
        }
        return true;
    }

    public boolean deleteUsers(User... users) throws DBException {
        if (users == null || Arrays.stream(users).anyMatch(Objects::isNull)) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        try (PreparedStatement statement = con.prepareStatement("DELETE FROM USERS WHERE ID = ?")) {
            for (User user : users) {
                statement.setInt(1, user.getId());
                statement.addBatch();
            }
            int[] result = statement.executeBatch();
            for (int rs : result) {
                if (rs <= 0) {
                    con.rollback();
                    return false;
                }
            }
            return true;
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            throw new DBException("Failed to delete user", e);
        }
    }

    public User getUser(String login) throws DBException {
        if (login == null) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        try (PreparedStatement statement = con.prepareStatement("SELECT * FROM USERS WHERE LOGIN = ?")) {
            statement.setString(1, login);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final int userId = resultSet.getInt("ID");
                final String userName = resultSet.getString("LOGIN");
                User user = User.createUser(userName);
                user.setId(userId);
                return user;
            }
        } catch (SQLException e) {
            throw new DBException("Failed to get user", e);
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
        if (name == null) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        try (PreparedStatement statement = con.prepareStatement("SELECT * FROM TEAMS WHERE NAME = ?")) {
            statement.setString(1, name);
            final ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                final int teamId = resultSet.getInt("ID");
                final String teamName = resultSet.getString("NAME");
                Team team = Team.createTeam(teamName);
                team.setId(teamId);
                return team;
            }
        } catch (SQLException e) {
            throw new DBException("Failed to get team", e);
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {
        final List<Team> all = new ArrayList<>();
        try (PreparedStatement statement = con.prepareStatement("SELECT * FROM TEAMS")) {
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                final int teamId = resultSet.getInt("ID");
                final String name = resultSet.getString("NAME");
                Team team = Team.createTeam(name);
                team.setId(teamId);
                all.add(team);
            }
            return all;
        } catch (SQLException e) {
            throw new DBException("Failed to get all team", e);
        }
    }

    public boolean insertTeam(Team team) throws DBException {
        if (team == null) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        try (PreparedStatement statement = con.prepareStatement(
                "INSERT INTO TEAMS (NAME) "
                        + "VALUES(?)",
                Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, team.getName());
            statement.executeUpdate();
            final ResultSet primaryKeys = statement.getGeneratedKeys();
            if (primaryKeys.next()) {
                team.setId(primaryKeys.getInt(1));
            }
        } catch (SQLException e) {
            throw new DBException("Failed to create new team", e);
        }
        return true;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        if (user == null || teams == null || Arrays.stream(teams).anyMatch(Objects::isNull)) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        try (final PreparedStatement statement = con.prepareStatement(
                "INSERT INTO USERS_TEAMS (USER_ID, TEAM_ID) "
                        + "VALUES(?,?)")){

            con.setAutoCommit(false);
            for (Team team : teams) {
                statement.setInt(1, user.getId());
                statement.setInt(2, team.getId());
                statement.addBatch();
            }
            int[] result = statement.executeBatch();
            for (int rs : result) {
                if (rs <= 0) {
                    con.rollback();
                    return false;
                }
            }
            con.commit();
            con.setAutoCommit(true);
        } catch (SQLException e) {
            try {
                con.rollback();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
            throw new DBException("Failed to create new TeamsForUser", e);
        }
        return true;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        if (user == null) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        final List<Team> all = new ArrayList<>();
        try (PreparedStatement statement = con.prepareStatement("SELECT TEAMS.ID, TEAMS.NAME " +
                "FROM USERS_TEAMS INNER JOIN USERS ON USERS_TEAMS.USER_ID = USERS.ID " +
                "INNER JOIN TEAMS ON USERS_TEAMS.TEAM_ID = TEAMS.ID  WHERE USERS.LOGIN = ?")) {
            statement.setString(1, user.getLogin());
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                final int teamId = resultSet.getInt("ID");
                final String name = resultSet.getString("NAME");
                Team team = Team.createTeam(name);
                team.setId(teamId);
                all.add(team);
            }
            return all;
        } catch (SQLException e) {
            throw new DBException("Failed to get teams for user", e);
        }
    }

    public boolean deleteTeam(Team team) throws DBException {
        if (team == null) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        try (PreparedStatement statement = con.prepareStatement("DELETE FROM TEAMS WHERE ID = ?")) {
            statement.setInt(1, team.getId());
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DBException("Failed to delete team", e);
        }
    }

    public boolean updateTeam(Team team) throws DBException {
        if (team == null) {
            throw new IllegalArgumentException(BAD_INPUT);
        }
        try (PreparedStatement statement = con.prepareStatement("UPDATE TEAMS SET NAME = ? WHERE ID = ?")) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DBException("Failed to update team", e);
        }
        return true;
    }
}
